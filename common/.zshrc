## Historique 
HISTFILE=~/.histfile
HISTSIZE=1000000
SAVEHIST=1000000
HISTCONTROL=ignoreboth
HISTTIMEFORMAT="%d/%m/%y %T "
HISTIGNORE='&:ls:ll:la:exit:clear:history'

# ALIAS
[ -f ~/.alias ] && . ~/.alias

# Path
[ -d ~/bin ] &&	export PATH=$PATH:~/bin
[ -d ~/.local/bin ] &&	export PATH=$PATH:~/.local/bin

autoload -U colors && colors

autoload -U bashcompinit && bashcompinit

export TERM="xterm-256color"

# Autocorrect
setopt correct
export SPROMPT="Correct $fg[red]%R$reset_color to $fg[green]%r$reset_color? [Yes, No, Abort, Edit] "

# Vi
bindkey -v
EDITOR="vim"

zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit
compinit
setopt COMPLETE_ALIASES

# try to avoid the 'zsh: no matches found...'
setopt nonomatch

# warning if file exists ('cat /dev/null > ~/.zshrc')
setopt NO_clobber

# Allow comments even in interactive shells
setopt interactivecomments

# ctrl-s will no longer freeze the terminal.
stty erase "^?"

# the default grml setup provides '..' as a completion. it does not provide
# '.' though. If you want that too, use the following line:
zstyle ':completion:*' special-dirs true

# compinit will not automatically find new executables in the $PATH. For example, after you install a new package, the files in /usr/bin/ would not be immediately or automatically included in the completion
zstyle ':completion:*' rehash true

# Debut/Fin de ligne
bindkey "^A" vi-beginning-of-line
bindkey "^E" vi-end-of-line

# Search
__fzf_use_tmux__() {
  [ -n "$TMUX_PANE" ] && [ "${FZF_TMUX:-0}" != 0 ] && [ ${LINES:-40} -gt 15 ]
}

__fzfcmd() {
  __fzf_use_tmux__ &&
    echo "fzf-tmux -d${FZF_TMUX_HEIGHT:-40%}" || echo "fzf"
}

# CTRL-R - Paste the selected command from history into the command line
fzf-history-widget() {
  local selected num
  setopt localoptions noglobsubst noposixbuiltins pipefail 2> /dev/null
  selected=( $(fc -rl 1 |
    FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} $FZF_DEFAULT_OPTS -n2..,.. --tiebreak=index --bind=ctrl-r:toggle-sort $FZF_CTRL_R_OPTS --query=${(qqq)LBUFFER} +m" $(__fzfcmd)) )
  local ret=$?
  if [ -n "$selected" ]; then
    num=$selected[1]
    if [ -n "$num" ]; then
      zle vi-fetch-history -n $num
    fi
  fi
  zle redisplay
  typeset -f zle-line-init >/dev/null && zle zle-line-init
  return $ret
}
zle     -N   fzf-history-widget
bindkey '^R' fzf-history-widget

## ctrl-q: quote line
mquote () {
	zle beginning-of-line
    zle forward-word
	# RBUFFER="'$RBUFFER'"
	RBUFFER=${(q)RBUFFER}
    zle end-of-line
}
zle -N mquote && bindkey '^q' mquote

# Ansible
export ANSIBLE_DISPLAY_OK_HOSTS="false"
export ANSIBLE_DISPLAY_SKIPPED_HOSTS="false"
export ANSIBLE_SHOW_CUSTOM_STATS="true"

# Zgenom
if [ -f $HOME/.zgenom/zgenom.zsh ]; then
  source $HOME/.zgenom/zgenom.zsh

  # Check for plugin and zgenom updates every 7 days
  # This does not increase the startup time.
  zgenom autoupdate

  # if the init script doesn't exist
  if ! zgenom saved; then
    zgenom load spaceship-prompt/spaceship-prompt spaceship
    zgenom load zsh-users/zsh-syntax-highlighting
  fi
fi

if [ -f $HOME/.spaceship.zsh ]; then
  source $HOME/.spaceship.zsh
fi
