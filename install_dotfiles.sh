#!/usr/bin/env bash

# install_dotfiles.sh
#-------------------------------------------------------------------------------

# VARIABLES
#-------------------------------------------------------------------------------

_dir="$(cd "$(dirname "$0")" && pwd)"
_commonDir="common"

[ ! -d "$_dir/$_commonDir" ] && echo "Common dir not found" && exit 1

_isRoot="false"
[ "$(id -u)" -eq 0 ] && _isRoot="true"

if ! [ -f /etc/os-release ]; then
  echo "/etc/os-release file missing"
  exit 1
fi

. /etc/os-release
_os="$ID"

export _dir _commonDir _os _distrib

case "$_os" in
  arch) _distrib="arch" ;;
  debian | ubuntu) _distrib="debian" ;;
esac

[ "$_distrib" = "" ] && echo "Distribution not found" && exit 2

# Vim
_vimDirectory="$HOME/.vim"

# Return Code
declare -i _rc=0

# FONCTIONS
#-------------------------------------------------------------------------------

_echo() {

  local level="$1"
  shift 1
  echo -e "[$(date +%F\ %T,%3N)] ${level}: $*"

}

_isInstall() {

  if [ "$1" ]; then
    if [ -x "$(command -v "$1")" ]; then
      return 0
    else
      return 1
    fi
  else
    _echo "ERROR" "Usage : _isInstall BIN"
  fi

}

# TRAITEMENT
#-------------------------------------------------------------------------------

_echo "INFO" "OS : $_os"
_echo "INFO" "Distribution family : $_distrib"

for bin in vim git stow; do
  if ! _isInstall "$bin"; then
    _echo "ERROR" "$bin must be installed"
    exit 3
  fi
done

if ! grep "^\[ -f ~/\.bashrc\.local \] && . ~/\.bashrc\.local$" "$HOME/.bashrc"; then
  _echo "INFO" "--> update $HOME/.bashrc"
  echo "[ -f ~/.bashrc.local ] && . ~/.bashrc.local" | tee -a "$HOME/.bashrc"
fi

if [ "$_isRoot" = "true" ]; then
  if ! grep "^\[ -f ~/\.bashrc\.local \] && . ~/\.bashrc\.local$" "$HOME/.profile"; then
    _echo "INFO" "--> update $HOME/.profile"
    echo "[ -f ~/.bashrc.local ] && . ~/.bashrc.local" | tee -a "$HOME/.profile"
  fi
fi

cd "$_dir" || exit
_echo "INFO" "===== Install dotfiles"
! [ -d "$HOME/.config" ] && mkdir -p "$HOME/.config"
stow --dir "$_dir" --target ~ common
_rc+=$?
stow --dir "$_dir" --target ~ "$_distrib"
_rc+=$?

_echo "INFO" "Install dotfiles completed"
if [ "$_rc" -eq 0 ]; then
  _echo "INFO" "No errors reported"
else
  _echo "WARN" "Errors reported (rc:$_rc)"
fi

# Install vim
_echo "INFO" "===== Install vim plugins"
_echo "INFO" "First run - autoinstall plugin manager"
vim -E -s -u "$HOME/.vimrc" +visual +qall &> /dev/null
_rc+=$?
_echo "INFO" "Second run - install all plugins"
vim -E -s -u "$HOME/.vimrc" +PlugInstall +visual +qall &> /dev/null
_rc+=$?

_echo "INFO" "Installed vim plugins completed"
if [ "$_rc" -eq 0 ]; then
  _echo "INFO" "No errors reported"
else
  _echo "WARN" "Errors reported (rc:$_rc)"
fi

_echo "INFO" "Installed zsh plugin manager"
if [ -d "${HOME}/.zgenom" ]; then
  _echo "INFO" "Remove previous zsh plugin manager installation"
  rm -rf "${HOME}/.zgenom"
fi
git clone https://github.com/jandamm/zgenom.git "${HOME}/.zgenom" &> /dev/null

_echo "INFO" "Installed zsh plugin manager completed"
if [ "$_rc" -eq 0 ]; then
  _echo "INFO" "No errors reported"
else
  _echo "WARN" "Errors reported (rc:$_rc)"
fi

_echo "INFO" "===== Installed completed"
if [ "$_rc" -eq 0 ]; then
  _echo "INFO" "No errors reported"
else
  _echo "WARN" "Errors reported (rc:$_rc)"
fi
exit $_rc
